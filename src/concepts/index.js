import React from "react";
import QuestionOptionContainer from "../common/question_option_container";
import { concepts_text } from "../constants/text";
import ConceptCard from "../common/concept-card";
import { getByDisplayValue } from "@testing-library/react";
import "./styles.css";
import QuestionContainer from "../common/question_container";
import { useNavigate } from "react-router-dom";
import { Button } from "@mui/material";
const Concepts = () => {
  let navigate = useNavigate()
  return (
    <div>
      <QuestionOptionContainer
        step_header={concepts_text.STEP_THREE_A}
        question={concepts_text.QUESTION_THREE_A}
      />
    
      <div style={{padding:"1rem",margin: "3rem 5rem"}}>
        <h1>Concetps Inspired with friends</h1>
        <h3>Expand concepts to shortlist</h3>
      <div className="idea-generator">
        <ConceptCard
          Heading="GroupFit Quest"
          Content="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        />
        <ConceptCard
          Heading="GroupFit Quest"
          Content="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        />
        <ConceptCard
          Heading="GroupFit Quest"
          Content="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        />
      </div>
      <QuestionContainer />
      </div>
      <Button variant="contained" style={{margin : "2rem 5rem"}} onClick={()=>{navigate("/user-stories")}}>
        Next
      </Button>
    </div>
  );
};

export default Concepts;
