import React from 'react'
import { user_stories_text } from '../constants/text'
import { Button, TextField, TextareaAutosize } from "@mui/material";
import { buttons } from "../constants/text";
import { Textarea } from "@mui/joy";
import "./styles.css"
const UserStories = () => {
  return (
    <div>
 <div className="step-container">
      <div className="step-header">{user_stories_text.STEP_FOUR}</div>
      <div className="step-question-container">
        <div className="step-question-header">{user_stories_text.QUESTION_FOUR}</div>
        <div className="step-question-body">
          <div>Industry:</div>
          <Textarea style={{ width: "70%", margin: " 0 2rem", overflow: "hidden" }} maxRows={1} />
        </div>
        <div className="step-question-body">
          <div>Solution:</div>
          <Textarea style={{ width: "70%", margin: " 0 2rem", overflow: "hidden" }} maxRows={1} />
        </div>
        <div className="step-question-body">
          <div>Feature:</div>
          <Textarea style={{ width: "70%", margin: " 0 2rem", overflow: "hidden" }} maxRows={1} />
        </div>
        <div className="step-question-body">
          <div>User:</div>
          <Textarea style={{ width: "70%", margin: " 0 2rem", overflow: "hidden" }} maxRows={1} />
        </div>
        <div className="step-question-body">
          <div># Stories:</div>
          <Textarea style={{ width: "70%", margin: " 0 2rem", overflow: "hidden" }} maxRows={1} />
        </div>
        <div className="step-question-body">
          <div>Tech Stack:</div>
          <Textarea style={{ width: "70%", margin: " 0 2rem", overflow: "hidden" }} maxRows={1} />
        </div>
        <div className="step-question-body">
          <div>Tools:</div>
          <Textarea style={{ width: "70%", margin: " 0 2rem", overflow: "hidden" }} maxRows={1} />
        </div>
      </div>
      <div className="step-question-button">
        <Button variant="contained">
          {buttons.SYNTHESIZE_CLIENT_SITUATION}
        </Button>
      </div>
    </div>    </div>
  )
}

export default UserStories