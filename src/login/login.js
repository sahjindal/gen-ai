import { Button, TextField } from "@mui/material";
import React from "react";
import "./login.css";
import { useNavigate } from "react-router-dom";

const Login = () => {
  let navigate = useNavigate();
  return (
    <div className="login-container">
      <TextField id="outlined-basic" label="User Name" variant="outlined" />
      <TextField id="outlined-basic" label="Password" variant="outlined" />
      <Button variant="contained" onClick={()=>navigate("/client")}>
        Submit
      </Button>
    </div>
  );
};

export default Login;
