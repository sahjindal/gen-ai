export const client_text = {
    STEP_ONE: "Step 1 : Client Situation Analysis",
    QUESTION_ONE: "Input brief client details to synthesize a comprehensive summary of their situation, unveiling customer needs and industry dynamics. Add details about their business customers, industry, competitors, challenges and goals as relevant. This rapid analysis saves hours on research and generates strategic 'How Might We' questions, aligning your project with client needs from the get-go.",
    DISPLAY_SYNTHESIS: "The synthesis will be displayed here. Edit & approve as this content will uniform outputs.",
    STEP_TWO_A: "Step 2a : Interview Insights",
    QUESTION_TWO_A: "Enter one interview transcript at a time to distill key insights, such as pain points, high points, and opportunities. If you have more than one interview, create the summary first then paste the next interview here (and repeat). This feature not only saves times but also lets you tailor the insights for a more focused analysis, ensuring your team's efforts are concentrated on sense making."
 };

export const users_text = {
    STEP_TWO_A: "Step 2a : Interview Insights",
    QUESTION_TWO_A: "Enter one interview transcript at a time to distill key insights, such as pain points, high points, and opportunities. If you have more than one interview, create the summary first then paste the next interview here (and repeat). This feature not only saves times but also lets you tailor the insights for a more focused analysis, ensuring your team's efforts are concentrated on sense making.",
    QUESTION_TWO_A_SUMMARY_HEADER : "The summary will be structured in the default format below. If you want to customize the synthesis categories or have unique queries, please modify the format.",  
    STEP_TWO_B: "Step 2b : Interview Theme Finder",
    QUESTION_TWO_B: "Find common and contrasting themes across multiple interviews, and derive one (or more) unique personas from the data. Produced with a transparent attribution trail back to individual interviews, this feature speeds up time-to-insight and sense making.",
    STEP_TWO_C: "Step 2c : User Personas",
    QUESTION_TWO_C: "Create detailed user personas based on the client situation and interview synthesis, ecompassing demographiics, goals, motivations, and behaviours. This process is vital for crafting targeted solutions that are deeply aligned with user goals and motivations.",
    STEP_TWO_D: "Step 2d  : Journey Mapping",
    QUESTION_TWO_D: "Develop an in-depth user journey map, illustrating customer experiences, emotions, and opportunities at interaction point. This map is infomred by client synthesis, interview insights and persona data, helping your team rapidly indentify key areas for enhancing the overall customer experience.",
    
};

export const concepts_text = {
    STEP_THREE_A: "Step 3a : Concept Ideation",
    QUESTION_THREE_A : "Develop multiple product or service concepts based on user needs and pain points. This feature enables you to generate a range of innovative ideas, shortlisting and refining the most promising ones, thus driving creativity with user expectationsc. Enter your own ideas or autogenerate based on the context above."
};

export const user_stories_text = {
    STEP_FOUR: "Step 4 : User Stories",
    QUESTION_FOUR: "Generate detailed user stories and acceptance criteria for features on your roadmap, customizable to different user personas. Then create tasks for every role on the team, tailored to your technology stack and productivity tools. This feature vastly reduces time spent writing and coordinating, and boosts time being productive, allowing for quicker alignment with the client and development team."
};

export const buttons = {
    SYNTHESIZE_CLIENT_SITUATION: "synthesize client situation",
};