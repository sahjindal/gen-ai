import React from "react";
import "./client.css";
import QuestionContainer from "../common/question_container";
import { client_text } from "../constants/text";
import { Button } from "@mui/material";
import { useNavigate } from "react-router-dom";
const Client = () => {
  let navigate= useNavigate();
  return (
    <div className="client-container">
      <QuestionContainer
        step_header={client_text.STEP_ONE}
        question={client_text.QUESTION_ONE}
      />
      <Button style={{ marginTop: "4rem" }} variant="contained" onClick={()=>navigate("/users")}>
        Next
      </Button>
    </div>
  );
};

export default Client;
