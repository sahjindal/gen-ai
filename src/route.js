import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Users from './users/users'
import Login from './login/login'
import Client from './client/client'
import Concepts from './concepts'
import UserStories from './user_stories'

const RouterComponent = () => {
  return (
<BrowserRouter>
<Routes>
    <Route path='/' element={<Login/>} />   
    <Route path='/client' element={<Client/>} />
    <Route path='/users' element={<Users/>} />
    <Route path='/concepts' element={<Concepts />} />
    <Route path='/user-stories' element={<UserStories />} />
    </Routes>
</BrowserRouter>
  )
}

export default RouterComponent