import "./App.css";
import RouterComponent from "./route";

function App() {
  return (
    <div className="App">
     <RouterComponent />
    </div>
  );
}

export default App;
