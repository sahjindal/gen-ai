import React from "react";
import QuestionContainer from "../common/question_container";
import { users_text } from "../constants/text";
import { Button } from "@mui/material";
import { useNavigate } from "react-router-dom";

const Users = () => {
  let navigate = useNavigate()
  return (
    <div>
      <QuestionContainer
        step_header={users_text.STEP_TWO_A}
        question={users_text.QUESTION_TWO_A}
        summary={true}
        summary_header={users_text.QUESTION_TWO_A_SUMMARY_HEADER}
      />

      <QuestionContainer
        step_header={users_text.STEP_TWO_B}
        question={users_text.QUESTION_TWO_B}
      />

      <QuestionContainer
        step_header={users_text.STEP_TWO_C}
        question={users_text.QUESTION_TWO_C}
      />

      <QuestionContainer
        step_header={users_text.STEP_TWO_D}
        question={users_text.QUESTION_TWO_D}
      />

      <Button variant="contained" style={{margin : "2rem 5rem"}} onClick={()=>{navigate("/concepts")}}>
        Next
      </Button>
    </div>
  );
};

export default Users;
