import React from "react";
import { Button, TextField, TextareaAutosize } from "@mui/material";
import { buttons } from "../../constants/text";
import "./styles.css";
import { Textarea } from "@mui/joy";
const QuestionOptionContainer = ({ step_header, question }) => {
  return (
    <div className="step-container">
      <div className="step-header">{step_header}</div>
      <div className="step-question-container">
        <div className="step-question-header">{question}</div>
        <div className="step-question-body">
          <div>hello</div>
          <Textarea style={{ width: "70%", margin: " 0 2rem", overflow: "hidden" }} maxRows={1} />
          <Button variant="contained">Auto Generate</Button>
        </div>
        <div className="step-question-body">
          <div>hello</div>
          <Textarea style={{ width: "70%", margin: " 0 2rem", overflow: "hidden" }} maxRows={1} />
          <Button variant="contained">Auto Generate</Button>
        </div>
        <div className="step-question-body">
          <div>hello</div>
          <Textarea style={{ width: "70%", margin: " 0 2rem", overflow: "hidden" }} maxRows={1} />
          <Button variant="contained">Auto Generate</Button>
        </div>
      </div>
      <div className="step-question-button">
        <Button variant="contained">
          {buttons.SYNTHESIZE_CLIENT_SITUATION}
        </Button>
      </div>
    </div>
  );
};

export default QuestionOptionContainer;
