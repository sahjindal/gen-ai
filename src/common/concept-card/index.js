import { Button } from '@mui/material'
import React from 'react'
import "./styles.css"

const ConceptCard = ({Heading, Content}) => {
  return (
    <div className='card-container'>
        <h3>
            {Heading}
        </h3>
        <div>
            {Content}
        </div>
        <div style={{margin:"2rem 0"}}>
            <Button variant='contained'>Expand</Button>
        </div>
    </div>
  )
}

export default ConceptCard