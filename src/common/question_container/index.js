import React, { useState } from "react";
import "./styles.css";
import { Button, TextareaAutosize } from "@mui/material";
import { Textarea } from "@mui/joy";
import { buttons, client_text } from "../../constants/text";

const QuestionContainer = ({
  step_header,
  question,
  summary_header,
  summary,
}) => {
  const [displaySynthesis, setDisplaySynthesis] = useState();
  return (
    <div className="step-container">
      <div className="step-header">{step_header}</div>
      <div className="step-question-container">
        <div className="step-question-header">{question}</div>
        <div className="step-question-body">
          <Textarea
            name="Solid"
            placeholder="Type in here…"
            variant="outlined"
            minRows={4}
            maxRows={4}
            style={{ overflow: "hidden" }}
          />
        </div>
        {summary && (
          <div className="step-question-container">
            <div className="step-question-header">{summary_header}</div>
            <div className="step-question-body">
              <Textarea
                name="Solid"
                placeholder="Type in here…"
                variant="outlined"
                minRows={10}
                maxRows={10}
                style={{ overflow: "hidden" }}
              />
            </div>
          </div>
        )}
      </div>
      <div className="step-question-button">
        <Button variant="contained" 
        // onClick={setDisplaySynthesis(true)}
        >
          {buttons.SYNTHESIZE_CLIENT_SITUATION}
        </Button>
      </div>

    { displaySynthesis===true &&( <div className="step-question-container">
        <div className="step-question-header">
          {client_text.DISPLAY_SYNTHESIS}
        </div>
        <div className="step-question-body">
          <Textarea
            name="Solid"
            placeholder="Type in here…"
            variant="outlined"
            minRows={10}
          />
        </div>
      </div>)}
    </div>
  );
};

export default QuestionContainer;
